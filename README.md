# Spigot
A Docker image for running Spigot.

[![build status](https://gitlab.com/docker-minecraft/spigot/badges/master/build.svg)](https://gitlab.com/docker-minecraft/spigot/commits/master)

# Installation
Here are the available versions of building Spigot:
* latest
* 1.11
* 1.10
* 1.9.4
* 1.9.2
* 1.9
* 1.8.8
* 1.8.7
* 1.8.3
* 1.8

Let's say we're running Minecraft version 1.11, simply clone and build the image:

```
$ git clone git@gitlab.com:docker-minecraft/spigot.git
$ cd spigot
$ docker build -t docker-minecraft/spigot:1.11 --build-arg SPIGOT_VERSION=1.11 src
$ docker run -p 25577:25577 -e "JAVA_ARGS=-Xmx1G" -v $(pwd)/my-spigot:/data -itd --name my-spigot docker-minecraft/spigot
$ docker attach my-spigot
```